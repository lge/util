var util = require('underscore');
  
function loadjscssfile(filename, filetype) {
	if (filetype == "js") {//if filename is a external JavaScript file
		var fileref = document.createElement('script');
		fileref.setAttribute("type", "text/javascript");
		fileref.setAttribute("src", filename);
	} else if (filetype == "css") {//if filename is an external CSS file
		var fileref = document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", filename);
	}
	if ( typeof fileref != "undefined")
		document.getElementsByTagName("head")[0].appendChild(fileref);
}

function checkType(typePatternArray, args) {
    var arr = Array.prototype.slice.call(args);
    if (arr.length < typePatternArray.length) {
        return false;
    }
    var typeString = args.map(function(el) {
        return typeof el;
    });
    var i, len = typePatternArray.length, valid = true, pattern = '', types;

        for (i = 0; i < len; ++i) {
            pattern = typePatternArray[i].split(':')[0];
            types = pattern.split('/');
            var tmp = false;
            if (types.length === 1 && types[0] === 'mixed') {
                tmp = true;
            } else {
                var j, len2 = types.length;
                for (j = 0; j < len2; ++j) {
                    if (types[j] === typeString[i]) {
                        tmp = true;
                        break;
                    }
                }
            }
            if (tmp === false) {
                valid = false;
                break;
            }
        }
    return valid;
}

// check whethre the cfg object has all required field defined by arr
function checkRequired(cfg, arr) {
    return util.all(arr, function(e) {
        return util.has(cfg, e);
    });
}

function findIndex(arr, obj, fromIndex) {
    if (fromIndex == null) {
        fromIndex = 0;
    } else if (fromIndex < 0) {
        fromIndex = Math.max(0, arr.length + fromIndex);
    }
    for (var i = fromIndex, j = arr.length; i < j; i++) {
        if (arr[i] === obj)
            return i;
    }
    return -1;
};

function findKey(store, obj) {
	for (var key in store) {
        if (store[key] === obj)
            return key;
    }
    return null;
};

function clickCoordsWithinElement(event) {
    var coords = { x: 0, y: 0};
    if (!event) {
        event = window.event;
        coords.x = event.x;
        coords.y = event.y;
    } else {
        var element = event.target ;
        var totalOffsetLeft = 0;
        var totalOffsetTop = 0 ;

        while (element.offsetParent)
        {
            totalOffsetLeft += element.offsetLeft;
            totalOffsetTop += element.offsetTop;
            element = element.offsetParent;
        }
        coords.x = event.pageX - totalOffsetLeft;
        coords.y = event.pageY - totalOffsetTop;
    }
    return coords;
}

function get_appropriate_ws_url() {
	var pcol;
	var u = document.URL;
	if(u.substring(0, 5) == "https") {
		pcol = "wss://";
		u = u.substr(8);
	} else {
		    pcol = "ws://";
		if(u.substring(0, 4) == "http") {
			u = u.substr(7);
		}
	}
	u = u.split('/');
	return pcol + u[0];
}

function apply(object, config, defaults) {
	if(defaults) {
		apply(object, defaults);
	}
	if(object && config && typeof config === 'object') {
		for(var key in config) {
			object[key] = config[key];
		}
	}
	return object;
};

function applyIf(object, config) {
	var property, undefined;

	if(object) {
		for(property in config) {
			if(object[property] === undefined) {
				object[property] = config[property];
			}
		}
	}

	return object;
};

// Merge objects to the first one recursively. The letter one overrides first one's value.
function merge(object, config, defaults) {
    if(defaults) {
		merge(object, defaults);
	}        
	if(object && config && typeof config === 'object') {
		for(var key in config) {
			if(util.isObject(config[key]) || util.isArray(config[key])) {

				if(!util.isDefined(object[key]) || object[key].constructor != config[key].constructor) {
					object[key] = config[key].constructor.call(typeof(window) === 'undefined' ? global : window);
				}
				merge(object[key], config[key]);
			} else {
				object[key] = config[key];
			}

		}
	}
	return object;
};

function isDefined(v) {
	return typeof v !== 'undefined';
};

function inherits(ctor, superCtor) {
    ctor.super_ = superCtor;
    ctor.prototype = Object.create(superCtor.prototype, {
        constructor: {
            value: ctor,
            enumerable: false,
            writable: true,
            configurable: true
        }
    });
};

util.mixin({
    apply : apply,
    applyIf : applyIf,
    loadjscssfile : loadjscssfile,
    merge : merge,
    isDefined : isDefined,
    inherits : inherits,
    clickCoordsWithinElement : clickCoordsWithinElement,
    get_appropriate_ws_url : get_appropriate_ws_url,
    checkRequired : checkRequired
});


module.exports = util;